#include"cbuffer.h"
cbuffer_t cb;
uint8_t cb_buff[100];
uint8_t a;
int main(void)
{
    cb_init(&cb, cb_buff, 7);
    uint8_t a[5] = { 0, 1, 2, 3, 4 };
    ////////////////////viết 5 giá trị vào cbuffer
    cb_write(&cb, a, 5);
    printf("vi tri cua writer:");
    printf("  %d", cb.writer);
    printf("\ngia tri trong cbuffer:");
    for (int i = 0; i < cb.size; i++)
    {
        printf("    %d", cb.data[i]);
    }
    //////////////////////Đọc 3 giá trị của cbuffer

    uint8_t b[3] = { 0, 0, 0 };
    printf("\nvi tri cua reader:");
    cb_read(&cb, b, 3);
    printf("  %d", cb.reader);
    printf("\ngia tri sau khi doc tu cbuffer:");
    for (int i = 0; i < 3; i++)
    {
        printf("     %d", b[i]);
    }
    ////////////////////viết thêm 5 giá trị vào cbuffer
    cb_write(&cb, a, 5);
    printf("\nvi tri cua writer:");
    printf("  %d", cb.writer);
    printf("\ngia tri trong cbuffer:");
    for (int i = 0; i < cb.size; i++)
    {
        printf("    %d", cb.data[i]);
    }
    ////////////////đọc 3 giá trị từ cbuffer
    printf("\nvi tri cua reader:");
    cb_read(&cb, b, 3);
    printf("    %d", cb.reader);
    printf("\ngia tri sau khi doc tu cbuffer:");
    for (int i = 0; i < 3; i++)
    {
        printf("     %d", b[i]);
    }
    ////////////////////viết thêm 5 giá trị vào cbuffer
    cb_write(&cb, a, 5);
    printf("\nvi tri cua writer:");
    printf("  %d", cb.writer);
    printf("\ngia tri trong cbuffer:");
    for (int i = 0; i < cb.size; i++)
    {
        printf("    %d", cb.data[i]);
    }
    
    //////////////////////////////////////////////Reset cirbular buffer
    cb_clear(&cb);
    printf("\ngia tri trong cbuffer:");
    for (int i = 0; i < cb.size; i++)
    {
        printf("    %d", cb.data[i]);
    }
    return 0;
}
